<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'word');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@|s{kMd[|R{*x?WS%>t{_83YY:L9>!V`&&,(mOvrE;ku|`IQw/i`(.-^b,+ew~@%');
define('SECURE_AUTH_KEY',  'r-~]5&v-ZOrfc5y QrNMb{Wd`^M Mo%}sXXp2@>R~[+*/Z#KcMCH+~5[0sFq+B-M');
define('LOGGED_IN_KEY',    '_~4Bn>jX+TmP3D$0HB(axK?pa=xdb$2IprERJU^g&Nd]VksgJ4>t(MSQ!@DLh9rD');
define('NONCE_KEY',        'n-`iS?93|:K34TZ%a{5-oSCJzUah@?(<S5QO9J:8h;!fG.aWQuR~6 I;4DAYICG4');
define('AUTH_SALT',        '(_d|v}DN*En,u0T)iTQ[ehM+js;vIWb6Rtn6a`=|EFSC!,Neqg+d+`YV*D`PA$-y');
define('SECURE_AUTH_SALT', 'O,.|O_;+M&>yOp4{`I7TqktmW1md:#Qad$GLkR+0+xk_,T@.w3l+<XXMA+H;wH-u');
define('LOGGED_IN_SALT',   '!go)lDAkQPxeHnAO>L)MJJc:n:lZ(Sw*?z{X+Rag;Nsjq?ITVMt)dGzVKv1>V}v:');
define('NONCE_SALT',       '|aET`F?_M:J!Br 1X_+--Z!]/[@9=il5XAp5NcxwW5%u9RiKI7*L^yh7K|Bsqn?F');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
